# Use an official OpenJDK runtime as a parent image
FROM openjdk:17-jdk-slim

# Set the working directory in the container
WORKDIR /app

# Copy the application JAR file into the container at /app
COPY target/demo-0.0.1-SNAPSHOT.jar /app/application.jar

# Change ownership of the application JAR file to the non-root user
RUN chmod +x /app/application.jar

# Add a non-root user
RUN useradd -r -s /bin/bash nruser

# Make port 8080 available to the world outside this container
EXPOSE 8080

# Switch to the non-root user
USER nruser

# Run the Spring Boot application when the container launches
CMD ["java", "-jar", "application.jar"]

