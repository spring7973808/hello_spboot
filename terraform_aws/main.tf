# Declare the provider and region
provider "aws" {
  region = "us-east-1" # Change this to your desired region
}

# Create a VPC
resource "aws_vpc" "jenkins_vpc" {
  cidr_block = "10.0.0.0/16"
  enable_dns_support = true
  enable_dns_hostnames = true
}

# Create an internet gateway
resource "aws_internet_gateway" "jenkins_igw" {
  vpc_id = aws_vpc.jenkins_vpc.id
}

# Create a subnet
resource "aws_subnet" "jenkins_subnet" {
  vpc_id     = aws_vpc.jenkins_vpc.id
  cidr_block = "10.0.1.0/24"
}

# Create a route table
resource "aws_route_table" "jenkins_route_table" {
  vpc_id = aws_vpc.jenkins_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.jenkins_igw.id
  }
}

# Associate the route table with the subnet
resource "aws_route_table_association" "jenkins_route_table_association" {
  subnet_id      = aws_subnet.jenkins_subnet.id
  route_table_id = aws_route_table.jenkins_route_table.id
}

# Create a security group allowing SSH access only from your IP address and HTTP from anywhere
resource "aws_security_group" "jenkins_sg" {
  name        = "jenkins-sg"
  description = "Security group for Jenkins instance"
  vpc_id      = aws_vpc.jenkins_vpc.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"] # Replace YOUR_IP_ADDRESS with your actual IP address
  }

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# Create ECR repository
resource "aws_ecr_repository" "jenkins_ecr_repo" {
  name = "jenkins-repo"  # Update with your desired repository name
}

# Create IAM role for Jenkins EC2 instance
resource "aws_iam_role" "jenkins_ec2_role" {
  name = "jenkins-ec2-role"  # Update with your desired role name

  assume_role_policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        "Effect" : "Allow",
        "Principal" : {
          "Service" : "ec2.amazonaws.com"
        },
        "Action" : "sts:AssumeRole"
      }
    ]
  })
}

# Attach policy to IAM role to allow access to ECR
resource "aws_iam_policy_attachment" "jenkins_ec2_policy_attachment" {
  name       = "jenkins-ec2-policy-attachment"
  roles      = [aws_iam_role.jenkins_ec2_role.name]
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"  # Policy for read-only access to ECR
}

# Create IAM instance profile and associate with IAM role
resource "aws_iam_instance_profile" "jenkins_ec2_instance_profile" {
  name = "jenkins-ec2-instance-profile"  # Update with your desired instance profile name
  role = aws_iam_role.jenkins_ec2_role.name
}

# Create an EC2 instance within the new VPC
resource "aws_instance" "jenkins_instance" {
  ami                    = "ami-0440d3b780d96b29d" # Change this to your desired AMI ID
  instance_type          = "t2.micro"            # Change this to your desired instance type
  associate_public_ip_address = true
  subnet_id              = aws_subnet.jenkins_subnet.id # Reference the ID of the newly created subnet
  key_name               = "divakshitest"
  vpc_security_group_ids = [aws_security_group.jenkins_sg.id] # Associate the instance with the security group

  tags = {
    Name = "jenkins-instance"
  }

  # User data to install Jenkins
  user_data = <<-EOF
              #!/bin/bash
              sudo yum update -y  # updates the package list and upgrades installed packages on the system
              sudo wget -O /etc/yum.repos.d/jenkins.repo https://pkg.jenkins.io/redhat-stable/jenkins.repo  #downloads the Jenkins repository configuration file and saves it to /etc/yum.repos.d/jenkins.repo
              sudo rpm --import https://pkg.jenkins.io/redhat-stable/jenkins.io-2023.key  #imports the GPG key for the Jenkins repository. This key is used to verify the authenticity of the Jenkins packages
              sudo yum upgrade -y #  upgrades packages again, which might be necessary to ensure that any new dependencies required by Jenkins are installed
              sudo dnf install java-11-amazon-corretto -y  # installs Amazon Corretto 11, which is a required dependency for Jenkins.
              sudo yum install jenkins -y  #installs Jenkins itself
              sudo yum install git* -y
              sudo yum install -y docker
              sudo systemctl start docker
              sudo systemctl enable docker
              sudo usermod -aG docker ec2-user
              sudo systemctl restart docker
              sudo systemctl enable jenkins  #enables the Jenkins service to start automatically at boot time
              sudo systemctl start jenkins
              sudo usermod -aG docker jenkins
              EOF
 }
# Output the public IP address of the Jenkins instance
output "jenkins_public_ip" {
  value = aws_instance.jenkins_instance.public_ip
}

